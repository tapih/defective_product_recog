import os
import json
import pandas as pd
import numpy as np

from tqdm import tqdm
from keras import backend as K
from keras.models import load_model
from keras.preprocessing.image import img_to_array, load_img

from train import make_threshold_path, make_checkpoint_path, score_to_class
from config import load_config
from constants import IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL, THRESHOLD_FIELD


def create_dataset(
    root_dir,
    img_height=IMG_HEIGHT,
    img_width=IMG_WIDTH,
):
    filenames = os.listdir(root_dir)
    num_data = len(filenames)
    img_size = (img_height, img_width)

    X = np.zeros((num_data, img_height, img_width, IMG_CHANNEL))
    for i, fname in tqdm(enumerate(filenames), total=num_data):
        path = os.path.join(root_dir, fname)
        img = load_img(path, target_size=img_size)
        X[i, :, :, :] = img_to_array(img)
    return X, filenames


class Ensemble:
    def __init__(self, model_root_dir, num_splits, threshold=0.5):
        self.models = []
        self.threshold = threshold
        for cv_fold in range(num_splits):
            self.models.append(
                load_model(
                    make_checkpoint_path(model_root_dir, cv_fold)
                )
            )

    def update_threshold(self, threshold):
        self.threshold = threshold

    def predict(self, X):
        y_preds = []
        for i, model in enumerate(self.models):
            y_preds.append(model.predict(X)[:, 0])
        return score_to_class(np.mean(y_preds, axis=0), self.threshold)


if __name__ == "__main__":
    config = load_config('../configs/parameters.json')
    with open(make_threshold_path(config.output_dir)) as f:
        threshold = json.load(f)[THRESHOLD_FIELD]

    X, filenames = create_dataset(config.predict_dir)
    model = Ensemble(config.output_dir, config.num_splits, threshold)
    y_pred = model.predict(X)
    pd.DataFrame({
        'filename': filenames,
        'y': y_pred,
    }).to_csv(os.path.join(config.output_dir, 'predicted.csv'), index=False)
