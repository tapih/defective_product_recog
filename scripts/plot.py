import os
import pandas as pd
import numpy as np
import seaborn
import matplotlib.pylab as plt

from config import load_config
from train import make_prcurve_path

config = load_config('../configs/parameters.json')
df = pd.read_csv(make_prcurve_path(config.output_dir))
print(df)

# pr
plt.grid(True)
plt.xlabel('recall')
plt.ylabel('precision')
plt.xlim(0, 1.05)
plt.ylim(0, 1.05)
plt.xticks(np.arange(0, 1.1, 0.1))
plt.yticks(np.arange(0, 1.1, 0.1))
plt.plot(df.recall, df.precision)
plt.savefig(os.path.join(config.figure_dir, 'prcurve.png'))
plt.close()

# epochs


def make_graph_by_epochs(csv_list, ylabel_name, img_name):
    df = pd.DataFrame()
    for i, csv_name in enumerate(csv_list):
        df_tmp = pd.read_csv(os.path.join(config.output_dir, csv_name))
        df[f'fold{i + 1}'] = df_tmp.Value
    plt.grid(True)
    plt.xlabel('epochs')
    plt.ylabel(ylabel_name)
    plt.ylim(0, 1.05)
    plt.yticks(np.arange(0, 1.1, 0.1))

    plt.plot(df.index+1, df.fold1)
    plt.plot(df.index+1, df.fold2)
    plt.plot(df.index+1, df.fold3)

    plt.legend(['fold1', 'fold2', 'fold3'])
    plt.savefig(os.path.join(config.figure_dir, img_name))
    plt.close()


make_graph_by_epochs(
    [
        'run_fold_1-tag-val_binary_accuracy.csv',
        'run_fold_2-tag-val_binary_accuracy.csv',
        'run_fold_3-tag-val_binary_accuracy.csv',
    ],
    'accuracy',
    'accuracy.png',
)

make_graph_by_epochs(
    [
        'run_fold_1-tag-val_loss.csv',
        'run_fold_2-tag-val_loss.csv',
        'run_fold_3-tag-val_loss.csv',
    ],
    'logloss',
    'loss.png',
)
