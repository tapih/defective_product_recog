import json
from box import Box


def load_config(path):
    with open(path, 'r') as f:
        body = json.load(f)
    return Box(body)
