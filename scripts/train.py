import os
import json
import pandas as pd
import numpy as np

from datetime import datetime

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import (
    f1_score,
    precision_score,
    recall_score,
    accuracy_score,
)
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.optimizers import Adam
from keras.models import load_model, Sequential
from keras.layers import (
    Conv2D,
    Dropout,
    BatchNormalization,
    Dense,
    GlobalMaxPool2D,
)

from config import load_config
from dataset import make_dataset_path
from constants import (
    IMG_HEIGHT,
    IMG_WIDTH,
    IMG_CHANNEL,
    CHECKPOINT_FMT,
    THRESHOLD_FILENAME,
    THRESHOLD_FIELD,
)


def make_checkpoint_path(output_dir, cv_fold):
    return os.path.join(
        output_dir,
        CHECKPOINT_FMT.format(cv_fold+1),
    )


def make_threshold_path(output_dir):
    return os.path.join(output_dir, THRESHOLD_FILENAME)


def make_prcurve_path(output_dir):
    return os.path.join(output_dir, 'prcurve.csv')


def create_model(num_filters=32, dropout_rate=0.5, img_height=IMG_HEIGHT, img_width=IMG_WIDTH):
    model = Sequential()
    model.add(Conv2D(
        num_filters,
        (3, 3),
        activation='relu',
        strides=2,
        padding='same',
        input_shape=(img_height, img_width, IMG_CHANNEL),
    ))
    model.add(Conv2D(
        num_filters,
        (3, 3),
        activation='relu',
        strides=2,
        padding='same',
    ))
    model.add(BatchNormalization())
    model.add(Conv2D(
        num_filters*2,
        (3, 3),
        strides=2,
        padding='same',
        activation='relu',
    ))
    model.add(Conv2D(
        num_filters*2,
        (3, 3),
        strides=2,
        padding='same',
        activation='relu',
    ))
    model.add(BatchNormalization())
    model.add(Dense(num_filters*4, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(GlobalMaxPool2D())
    model.add(Dense(1, activation='sigmoid'))
    return model


def score_to_class(y_pred, threshold=0.5):
    return (y_pred > threshold) * 1


def print_scores(y_true, y_pred):
    metrics = {
        'pct_true': lambda y1, y2: np.sum(y2) / len(y2),
        'f1': f1_score,
        'precision': precision_score,
        'recall': recall_score,
        'accuracy': accuracy_score,
    }

    for key, metric in metrics.items():
        print("{}: {:.4f}".format(key.rjust(9), metric(y_true, y_pred)))


def compute_pr_curve(y_true, y_pred, num_points=20):
    thresholds = []
    precisions = []
    recalls = []
    for i in range(num_points):
        thresholds.append(i / num_points)
        precisions.append(
            precision_score(y_true, score_to_class(y_pred, thresholds[-1])))
        recalls.append(
            recall_score(y_true, score_to_class(y_pred, thresholds[-1])))
    return thresholds, precisions, recalls


def search_best_threshold(thresholds, precisions, recalls):
    prev_threshold, prev_recall = 0.0, 1.0
    best_threshold = 0.0
    for t, p, r in zip(thresholds, precisions, recalls):
        if prev_recall == 1 and r != 0:
            best_threshold = prev_threshold
        prev_threshold = t
        prev_recall = r
    return best_threshold


if __name__ == "__main__":
    config = load_config('../configs/parameters.json')
    tmp = np.load(make_dataset_path(config.output_dir, True))
    X, y = tmp['X_train'], tmp['y_train']
    X_test, y_test = tmp['X_test'], tmp['y_test']
    y_preds = []
    del tmp

    kfold = StratifiedKFold(
        n_splits=config.num_splits,
        shuffle=True,
        random_state=123,
    )

    # swap idx_train and idx_valid because dataset is too small
    for cv_fold, (idx_valid, idx_train) in enumerate(kfold.split(X, y)):
        X_train, y_train = X[idx_train], y[idx_train]
        X_valid, y_valid = X[idx_valid], y[idx_valid]
        model = create_model(
            num_filters=config.num_filters,
            dropout_rate=config.dropout_rate,
            img_height=config.img_square_resized,
            img_width=config.img_square_resized,
        )

        if cv_fold == 0:
            print(f'X_train: {X_train.shape}')
            print(f'y_train: {y_train.shape}')
            print(f'X_valid: {X_valid.shape}')
            print(f'y_valid: {y_valid.shape}')
            model.summary()

        # fit
        datagen = ImageDataGenerator(
            rotation_range=config.rotation_range,
            zoom_range=config.zoom_range,
            width_shift_range=config.width_shift_range,
            height_shift_range=config.height_shift_range,
            brightness_range=config.brightness_range,
        )

        checkpoint_path = make_checkpoint_path(config.output_dir, cv_fold)
        model.compile(
            optimizer=Adam(lr=config.learning_rate),
            loss='binary_crossentropy',
            metrics=['binary_accuracy'],
        )
        model.fit_generator(
            datagen.flow(
                X_train,
                y_train,
                batch_size=config.batch_size,
                shuffle=True,
            ),
            epochs=config.epochs,
            steps_per_epoch=config.steps_per_epoch,
            validation_data=(X_valid, y_valid),
            callbacks=[
                ModelCheckpoint(
                    checkpoint_path,
                    monitor='val_binary_accuracy',
                    mode='max',
                    save_best_only=True,
                    verbose=1,
                ),
                TensorBoard(
                    os.path.join(
                        config.tensorboard_log_dir,
                        f'fold_{cv_fold+1}',
                    ),
                    histogram_freq=1,
                    write_graph=True,
                ),
            ],
        )

        model = load_model(checkpoint_path)

        print('valid')
        print_scores(y_valid, score_to_class(model.predict(X_valid)))
        print('\n')

        print('test')
        y_preds.append(model.predict(X_test)[:, 0])
        print_scores(y_test, score_to_class(y_preds[-1]))
        print('\n')

        K.clear_session()

    # ensemble and print_scores
    print('ensembled')
    y_pred = np.mean(y_preds, axis=0)
    print_scores(y_test, score_to_class(y_pred))

    thresholds, precisions, recalls = compute_pr_curve(y_test, y_pred)
    pd.DataFrame({
        'threshold': thresholds,
        'precision': precisions,
        'recall': recalls,
    }).to_csv(make_prcurve_path(config.output_dir), index=False)

    threshold = search_best_threshold(thresholds, precisions, recalls)
    with open(make_threshold_path(config.output_dir), 'w') as f:
        json.dump({THRESHOLD_FIELD: threshold}, f)
