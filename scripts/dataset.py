import os
import numpy as np

from tqdm import tqdm
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array, load_img

from config import load_config
from constants import IMG_HEIGHT, IMG_WIDTH, IMG_CHANNEL, DATASET_NAME


def make_dataset_path(output_dir, with_ext=False):
    filename = DATASET_NAME + ('.npz' if with_ext else '')
    return os.path.join(output_dir, filename)


def create_dataset(
    ok_dir,
    ng_dir,
    test_fraction=0.1,
    img_height=IMG_HEIGHT,
    img_width=IMG_WIDTH,
    random_state=123,
):
    assert test_fraction > 0, "test_fraction should be > 0"
    assert test_fraction < 1, "test_fraction should be < 1"

    ok_img_paths = os.listdir(ok_dir)
    ng_img_paths = os.listdir(ng_dir)
    num_ok = len(ok_img_paths)
    num_ng = len(ng_img_paths)
    num_all = num_ok + num_ng
    img_size = (img_height, img_width)

    X = np.zeros((num_all, img_height, img_width, IMG_CHANNEL))
    y = np.zeros(num_all)

    print('{} OK images were fouond.'.format(num_ok))
    for i, fname in tqdm(enumerate(ok_img_paths), total=num_ok):
        path = os.path.join(ok_dir, fname)
        img = load_img(path, target_size=img_size)
        X[i, :, :, :] = img_to_array(img)
        # y[i] = 1

    print('{} NG images were fouond.'.format(num_ng))
    for i, fname in tqdm(enumerate(ng_img_paths), total=num_ng):
        path = os.path.join(ng_dir, fname)
        img = load_img(path, target_size=img_size)
        X[i + num_ok, :, :, :] = img_to_array(img)
        y[i + num_ok] = 1

    X_train, X_test, y_train, y_test = train_test_split(
        X,
        y,
        test_size=test_fraction,
        random_state=random_state,
        stratify=y,
    )
    return X_train, X_test, y_train, y_test


if __name__ == "__main__":
    config = load_config('../configs/parameters.json')
    X_train, X_test, y_train, y_test = create_dataset(
        os.path.join(config.input_dir, 'OK'),
        os.path.join(config.input_dir, 'NG'),
        test_fraction=config.test_fraction,
        img_height=config.img_square_resized,
        img_width=config.img_square_resized,
    )
    np.savez_compressed(
        make_dataset_path(config.output_dir, False),
        X_train=X_train,
        X_test=X_test,
        y_train=y_train,
        y_test=y_test,
    )

    print(f'X_train: {X_train.shape}')
    print(f'y_train: {y_train.shape}')
    print(f'X_test : {X_test.shape}')
    print(f'y_test : {y_test.shape}')
    print(f'         {sum(y_test)} (true samples)')
